function Que(maxLength) {
	
	var data = [];
	
	this.Enqueue = function(element) {
		if (data.length < maxLength) {
			data.push(element);
		} else {
			console.log('Que is full');
		}
	}
	
	this.Dequeue = function() {
		if (data.length > 0) {
			return data.shift();
		} else {
			console.log('Que is empty');
			return null;
		}
	}
	
}

var q = new Que(10);

for(var i = 0; i < 11; i++) {
	q.Enqueue(i);
}

for(var i = 0; i < 11; i++) {
	console.log(q.Dequeue());
}