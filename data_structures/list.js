function LinkedList(element) {
	var firstNode = new ListNode(element, null);
	var length = 1;
	
	this.Get = function(n) {
		if (n < 0 || n >= length) {
			console.log('Invalid position');
			return null;
		}
		return firstNode.Get(n);
	};
	
	this.InsertStart = function(x) {
		var temp = firstNode;
		firstNode = new ListNode(x, temp);
		return ++length;
	};
	
	this.InsertEnd = function(x) {
		firstNode.Insert(x);
		return ++length;
	};
	
	this.InsertAt = function(x, n) {
		if (n < 0 || n >= length) {
			console.log('Invalid position');
			return null;
		}
		firstNode.InsertAt(x, n);
		return ++length;
	};
	
	this.Set = function(x, n) {
		if (n < 0 || n >= length) {
			console.log('Invalid position');
			return null;
		}
		return firstNode.Set(x, n);
	};
	
	this.Delete = function(n) {
		if (n < 0 || n >= length) {
			console.log('Invalid position');
			return length;
		} else if (length === 0) {
			console.log('List is empty');
			return length;
		} else if (n === 0) {
			firstNode = firstNode.GetNext();
			return --length;
		} else {
			firstNode.Delete(n);
			return --length;
		}
	};
}

function ListNode(element, nextElement) {
	var data = element;
	var next = nextElement;
	
	this.Get = function(n) {
		return n === 0 ? data : next.Get(n - 1);
	};
	
	this.GetNext = function() {
		return next;
	};
	
	this.Insert = function(x) {
		if (next) return next.Insert(x);
		next = new ListNode(x, null);
	};
	
	this.InsertAt = function(x, n) {
		if (n === 0) {
			var tempNext = new ListNode(data, next);
			data = x;
			next = tempNext;
			return null;
		}
		return next.InsertAt(x, n - 1);
	};
	
	this.Set = function(x, n) {
		if (n === 0) {
			data = x;
			return null;
		}
		return next.Set(x, n - 1);
	};
	
	this.Delete = function(n) {
		if (n === 1) {
			next = next.GetNext();
			return null;
		}
		return next.Delete(n - 1);
	};
}

var l = new LinkedList(0);
for (var i = 1; i < 10; i++) {
	l.InsertStart(i);
}
l.InsertAt(100, 5);
l.Set(78,6);
l.Delete(0);
for (var i = 0; i < 12; i++) {
	console.log(l.Get(i));
}