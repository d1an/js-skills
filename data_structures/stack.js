function Stack(maxLength) {
	
	var data = [];
	
	this.Push = function(element) {
		if (data.length < maxLength) {
			data.push(element);
		} else {
			console.log('Stack is full');
		}
	}
	
	this.Pop = function() {
		if (data.length > 0) {
			return data.pop();
		} else {
			console.log('Stack is empty');
			return null;
		}
	}
	
}

var s = new Stack(10);

for(var i = 0; i < 11; i++) {
	s.Push(i);
}

for(var i = 0; i < 11; i++) {
	console.log(s.Pop());
}