// Immediately-Invoked-Function-Expression (IIFE)
var Singleton = (function() {
	var singletonInstance;
	
	function initialize() {
		var uniqueSingletonVariable = 1337;
		function set(value) {
			uniqueSingletonVariable = value;
		}
		function get() {
			return uniqueSingletonVariable;
		}
		console.log('Singleton object created');
		return {
			set: set,
			get: get
		};
	}
	
	return function() {
		if (!singletonInstance) {
			singletonInstance = initialize();
		}
		return singletonInstance;
	};
})();

var s1 = new Singleton();
var s2 = new Singleton();

s1.set(2007);
console.log(s1.get() === s2.get());