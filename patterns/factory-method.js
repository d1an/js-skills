function Phone() {
	var imei = 0;
	
	this.getName = function() {
		return "IPhone SE: " + imei;
	};
	
	this.setIMEI = function(number) {
		imei = number;
	};
}

function PhoneFactory() {
	this.getPhone = function() {
		return factoryMethod();
	};
	
	function factoryMethod() {
		var phone = new Phone();
		var randomIMEI = Math.floor((Math.random() * 89900) + 10000);
		phone.setIMEI(randomIMEI);
		return phone;
	}
}

var factory = new PhoneFactory();
var iphone = factory.getPhone();
console.log(iphone.getName());