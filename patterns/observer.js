function Observer(name) {
	var observerName = name;
	this.update = function(message) {
		console.log('Hi! My name is ', observerName, 
		'. A Subject told me: ', message);
	};
}

function Subject() {
	var observers = [];
	
	this.subscribe = function(observer) {
		observers.push(observer);	
	};
	
	this.unsubscribeAll = function() {
		observers = [];
	};
	
	this.notify = function(message) {
		for (var i = 0; i < observers.length; i++) {
			observers[i].update(message);
		}
	};
}

var newspaper = new Subject();

newspaper.subscribe(new Observer('Mike'));
newspaper.subscribe(new Observer('John'));

newspaper.notify('Donald Trump wins elections!');

newspaper.unsubscribeAll();
newspaper.notify('Nobody will see this :)');