function Audi() {
	this.getCost = function() {
		return 14599;
	};
	
	this.getEngineVolume = function() {
		return 2.6;
	};
}

// Decorator 1
function leatherSeats(car) {
	var cost = car.getCost();
	car.getCost = function() {
		return cost + 1299; 
	};
}

// Decorator 2
function bulletproofWindows(car) {
	var cost = car.getCost();
	car.getCost = function() {
		return cost + 8699; 
	};
}

var myAudi = new Audi();
leatherSeats(myAudi);
bulletproofWindows(myAudi);

console.log(myAudi.getCost());
console.log(myAudi.getEngineVolume());