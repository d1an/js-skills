function Subject() {
	var observers = [];
	
	this.subscribe = function(observer) {
		observers.push(observer);	
	};
	
	this.unsubscribeAll = function() {
		observers = [];
	};
	
	this.notifyAll = function() {
		observers.forEach(o => {
			o.notify();	
		});
	};
}

function Model() {
	var subject = new Subject();
	var data = [];
	
	this.getAll = function() {
		return data;
	};
	
	this.add = function(element) {
		data.push(element);
		subject.notifyAll();
	};
	
	this.delete = function(element) {
		var indexToDelete = data.indexOf(element);
		if (indexToDelete > 0) {
			data.splice(indexToDelete, 1);
			subject.notifyAll();
		}
	};
	
	this.subscribe = function(observer) {
		subject.subscribe(observer);
	};
}

function View(model) {
	var template = '[VIEW]\tMVC template [%s]';
	var dataToRender = [];
	
	function getData() {
		return model.getAll();
	}
	
	this.notify = function() {
		console.log('[VIEW]\tI have new data set to be rendered!');
		dataToRender = getData();
	};
	
	this.render = function() {
		console.log(template, dataToRender);
	};
}

function Controller(view, model) {
	model.subscribe(view);
	model.subscribe(this);
	
	this.renderView = function() {
		view.render();
	};
	
	this.additionEventHandler = function(data) {
		model.add(data);
	};
	
	this.deletionEventHandler = function(data) {
		model.delete(data);
	};
	
	this.notify = function() {
		console.log('[CTRL]\tNew data is here. I should re-render the view');
		this.renderView();	
	};
}

(function bootstrap() {
	var model = new Model();
	var view = new View(model);
	var controller = new Controller(view, model);
	
	controller.additionEventHandler('one');
	controller.additionEventHandler('two');
	controller.additionEventHandler('three');
})();

