function Hamburger(options) {
	this.sauce = options.sauce || 'hot chili';
	this.onion = options.onion || true;
	this.meat = options.meat || 'beef';
}

function Cheeseburger(options) {
	this.sauce = options.sauce || 'tar tar';
	this.meat = options.meat || 'pork';
	this.cheese = options.cheese || 'swiss';
}

function Factory() {
	// Default class
	this.burgerClass = Hamburger;
	this.createBurger = function(options) {
		switch(options.burgerType) {
			case 'Cheeseburger':
				this.burgerClass = Cheeseburger;
				break;
			case 'Hamburger':
				this.burgerClass = Hamburger;
				break;
		}
		return new this.burgerClass(options);
	};
	
	
}

var burgerFactory = new Factory();
var burger = burgerFactory.createBurger({
	burgerType: 'Hamburger',
	sauce: 'ayoli'
});

console.log(burger instanceof Hamburger);
console.log(burger);